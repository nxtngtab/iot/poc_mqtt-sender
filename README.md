**1a parte** ESP32 envío de datos via MQTT
===
En este proyecto se hace uso del sensor interno de temperatura del microcontrolador ESP32, se envían periódicamente las mediciones usando el protocolo MQTT. Esta es la **1a parte** de la PoC que en un 2o sistema recibe estos datos que son enviados a la tangle IOTA. Esta información queda datada y protegida de manipulaciones. Tendremos así un completo y robusto sistema Tamper-Proof.



Esquema eléctrico
---
El esquema eléctrico es muy simple, utiliza el sensor interno de temperatura del ESP32 y se añaden 2 leds para informar de que se están obteniendo errores (led rojo) y/o cuando se envían correctamente los datos (led verde).

![](doc/images/ESP32_IOTA_mqtt_POC/ESP32_IOTA_mqtt_POC.png)



Nuestro montaje
---
Montaje sencillo en unas protoboards. En la foto se puede ver en detalle como quedan ESP32, componentes y cableado

![](doc/images/ESP32_IOTA_mqtt_POC/ESP32_IOTA_mqtt_POC___senderProto.jpg)



Software implementado
---
El software ha de realizar las siguientes tareas:
* Conectarse a una red Wifi.
* Tomar datos periódicamente.
* Enviar datos periódicamente via MQTT.
* Reportar estados de errores en el led rojo.
* Reportar exito de envio de datos haciendo blink en led verde.
* En caso de errores realizar intentos de recuperacion/correccion.

Para este propósito se ha optado por utilizar el Sistema Operativo de Tiempo Real FreeRTOS. La implementación está en este repositorio y junto con el Esquema eléctrico es posible replicar fácilmente nuestra prueba.



Configuración
---
Para configurar el sistema hay que editar el fichero **main/config.h** de este repositorio, aqui se podra setear ...
* La configuración Wifi.
* La máquina, puerto, usuario y password para publicar vía MQTT en el 2o sistema que espera los mensajes con las mediciones (ya en la configuracion del proyecto hay ajustado un servidor público que da este servicio).
* El nombre del dispositivo (importante a la hora de dar permiso de recepción en el 2o).
* La latencia entre mediciones+envios.



**2a parte** Recepcion de datos via MQTT, realización de PoW y envío de datos a IOTA
===
Como ya hemos indicado, necesitaremos un servidor MQTT que espere el envío de mensajes desde la ESP32 y de un programa que ...
* Consuma/Reciba estos mensajes.
* Acepte o deniegue el trabajo en función del nombre del dispositivo en el mensaje.
* Realice una PoW para IOTA y envíe una transacción con el contenido del mensaje recibido, siguiendo el formato de mensajería ChannelsDeIOTA (que se apoya en el sistema de mensajes StreamsDeIOTA).

El sistema donde montar este servidor puede ser Linux, MacOS, Windows o incluso otro que permita montar un servidor MQTT y que este disponible como plataforma target por el lenguaje de programación Rust (en el cual está implementado el programaConsumidor).

El programaConsumidor esta disponible en https://github.com/iot2tangle/Streams-mqtt-gateway , como referencia indicamos que nosotros hemos utilizado una máquina raspberryPI 3, con sistema operativo ArchLinux y con servidor MQTT mosquitto.



Configuración
---
En el proyecto será necesario editar el fichero **config.json** para añadir los nombres de dispositivos de los que se aceptaran trabajos.

El servidor MQTT es necesario permitir los envíos anónimos ... para nuestro servidor mosquitto en el fichero de configuración hemos seteado las siguientes opciones de puerto y acceso.
``` 
listener 1883
allow_anonymous true
```
Y para publicar desde el proyecto cliente el usuario y password serán vacíos.



Verificacion de los datos escritos en la Tangle IOTA
===
Cuando arrancamos el programaConsumidor, genera el dato ROOT del ChannelDeIOTA que es necesario conocer para poder leer los mensajes escritos, con este dato podremos ir al servidor publico https://explorer.iot2tangle.io y leer los mensajes. En https://github.com/iot2tangle/Streams-explorer y en https://github.com/iot2tangle/streams-decoder tenemos el codigo fuente del servidor y una utilidad mas core de línea de comandos.

Aquí hemos publicado un vídeo donde hacemos uso de 2 ESP32 midiendo cada 20 segundos y reportando a un servidor que hemos montado en una raspberryPI 3. Posteriormente visitamos https://explorer.iot2tangle.io y verficamos que estamos publicando+asegurando los mensajes en la Tangle IOTA:

[![PoC IoT ESP32 to IOTA Tangle _via Channels using Streams_](https://img.youtube.com/vi/5R_bzo9_pr8/0.jpg)](https://www.youtube.com/watch?v=5R_bzo9_pr8)

En este segundo vídeo, mostramos una mejor visualización los mismos logs que se pueden ver en el vídeo anterior:

[![PoC IoT ESP32 to IOTA Tangle _via Channels using Streams_ Screens and Logs](https://img.youtube.com/vi/5-p-4xG0nWM/0.jpg)](https://www.youtube.com/watch?v=5-p-4xG0nWM)

