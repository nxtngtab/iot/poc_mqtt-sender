#ifndef _CONFIG_H
#define _CONFIG_H

/* ----- CONFIG FILE ----- */

/* Device */
const char* id_name = "ZZA";

/* Network Configuration */
const char* ssid_WiFi = "mySSID";
const char* pass_WiFi = "myPASS";

/* Broker Configuration */
const char* address = "mqtt.iot2tangle.link";  /* Broker address (MQTT), must NOT include 'http://xxx' or 'tcp://xxx' */
int port = 8883;
const char* topic = "iot2tangle";  /* MQTT topic */
const char* user = "mqtti2t";  /* MQTT user */
const char* password = "integrateeverything";  /* MQTT password */

/* Enable Sensors */
bool isEnable_TemperatureIntern = true; //TI = TemperatureIntern

/* Interval of time */
long interval = 20;    /* Time in seconds between */


#endif
