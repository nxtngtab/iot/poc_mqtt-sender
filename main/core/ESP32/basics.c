#include "basics.h"
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* ESP32 FreeRTOS DEPENDENCIES */
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "sdkconfig.h"

char buffer[100];
char *s;

void welcome_msg() {
  printf("\n                        ----  ESP32  --  IOT2TANGLE  --");

#ifdef HTTP
  printf("  HTTP  ----\n\n"); // Printf in shell
#endif
#ifdef MQTT
  printf("  MQTT  ----\n\n"); // Printf in shell
#endif
}

void d_collect_msg(long *n) {
  printf("\n>> ITERATION:: %ld", *n);
}

void print_json(char *js) {
  printf("\n>>>> DATA:: %s", js);
  printf("\n>>>>>> SENDING TO TANGLE...\n");
}

void udelay_basics(long d) {
  vTaskDelay((d / 1000) / portTICK_PERIOD_MS);
}

long take_time_basics() {
  time_t t;
  time(&t);
  return t;
}

void restart_basic() {
  esp_restart(); /* Reboot ESP32 */
}
