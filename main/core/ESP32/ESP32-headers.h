#ifndef ESP32_HEADERS_H
#define ESP32_HEADERS_H

#include "basics.h"
#include "peripherals/gpio/gpio.h"
#include "sensors/internal/internal.h"

#ifdef MICROCONTROLLER
#include "wifi/wifi.h"
#endif

#ifdef HTTP
#include "protocols/http/http.h"
#endif

#ifdef MQTT
#include "protocols/mqtt/mqtt.h"
#endif

#endif
