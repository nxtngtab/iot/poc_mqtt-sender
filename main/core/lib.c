#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "struct-device.h"
#include "config.h"
#include "devices.h"

void config(struct device *z) { //User assignments
  z->id = id_name;

  z->addr = address;
  z->addr_port = port;

#ifdef MQTT
  z->user_mqtt = user;
  z->pass_mqtt = password;
  z->top = topic;
#endif

#ifdef MICROCONTROLLER
  z->ssid_wifi = ssid_WiFi;
  z->pass_wifi = pass_WiFi;
#endif

  z->isEnable[0] = isEnable_TemperatureIntern; //TI = TemperatureIntern
  z->isEnable[1] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[2] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[3] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[4] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[5] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[6] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[7] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[8] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[9] = false;  //free, future sensor, future dataFrom sensor
  z->isEnable[10] = false; //free, future sensor, future dataFrom sensor
  z->isEnable[11] = false; //free, future sensor, future dataFrom sensor

  z->s_name[0] = "TI"; //TI = TemperatureIntern
  z->s_name[1] = "1";  //free, future sensor, future dataFrom sensor
  z->s_name[2] = "2";  //free, future sensor, future dataFrom sensor
  z->s_name[3] = "3";  //free, future sensor, future dataFrom sensor
  z->s_name[4] = "4";  //free, future sensor, future dataFrom sensor
  z->s_name[5] = "5";  //free, future sensor, future dataFrom sensor
  z->s_name[6] = "6";  //free, future sensor, future dataFrom sensor
  z->s_name[7] = "7";  //free, future sensor, future dataFrom sensor
  z->s_name[8] = "8";  //free, future sensor, future dataFrom sensor
  z->s_name[9] = "9";  //free, future sensor, future dataFrom sensor
  z->s_name[10] = "10";//free, future sensor, future dataFrom sensor
  z->s_name[11] = "12";//free, future sensor, future dataFrom sensor

  z->interv = interval;
}

void initPeripherals(long *c) {
  *c = 0; // Init counter

#ifdef SHELLPRINT
  welcome_msg(); // Printf in shell
#endif

  init_LEDs();
  init_internal(true);
}

void led_blinks(int led, int iter, int usec) // LED Blink function-> led: 0 Green LED, 1 Red LED - iter: iterations quantity - usec: delay time in usec
{
  int i;
  for (i = 0; i < iter; i++) {
    led_GPIO(led, 1);
    udelay_basics(usec);
    led_GPIO(led, 0);
    udelay_basics(usec);
  }
}

void connectNetwork(struct device *z, bool first_t) {
#ifdef MICROCONTROLLER
  if (first_t) {
    while (!connectAttempt(z->ssid_wifi, z->pass_wifi)) /* Attempt to connect to the network via WiFi, in RaspberryPi only check connection to the network. */
    {
      led_blinks(0, 1, 600000); // Blink in green GREEN - ERROR 0 (No WiFi connection);
      led_blinks(1, 1, 600000); // Blink in green RED - ERROR 0 (No WiFi connection);
    }
  }
#endif
  if (!init_socket(z->addr, z->addr_port, z->user_mqtt, z->pass_mqtt, first_t)) /* Check Endpoint */
  {
    udelay_basics(100000);
    led_blinks(1, 3, 70000); // Blink in green RED - ERROR 1 (Bad connection with the endpoint);
  }
}

void getData(struct device *z, long *c) {
  ++(*c);

#ifdef SHELLPRINT // Printf in shell
  d_collect_msg(c);
#endif

  /* GET DATA INTERNAL TEMPERATURE */
  strcpy(z->d[0], get_internal());
}

long take_time() {
  return take_time_basics();
}

char *take_timetoa() {
  char buffer[14];
  char *s;
  sprintf(buffer, "%lu", take_time_basics());
  s = buffer;
  return s;
}

void generateJson(struct device *z) {
  int i, aux = 0;
  strcpy(z->json, "{\"iot2tangle\":[");

  /* ADD DATA INTERNAL TEMPERATURE */
  strcat(z->json, "{\"sensor\":\"");
  strcat(z->json, z->id);
  strcat(z->json, "#");
  strcat(z->json, z->s_name[0]);
  strcat(z->json, "\",\"data\":[");
  for (i = 0; i < 1; i++) {
    if (z->isEnable[i + 0]) {
      if (aux != i) { strcat(z->json, ","); }
      strcat(z->json, "{\"id\":\"");
      strcat(z->json, z->id);
      strcat(z->json, "\"}");
        strcat(z->json, ",");
      strcat(z->json, "{\"type\":\"");
      strcat(z->json, z->s_name[i + 0]);
      strcat(z->json, "\"}");
        strcat(z->json, ",");
      strcat(z->json, "{\"time\":\"");
      strcat(z->json, take_timetoa());
      strcat(z->json, "\"}");
        strcat(z->json, ",");
      strcat(z->json, "{\"data\":\"");
      strcat(z->json, z->d[i + 0]);
      strcat(z->json, "\"}");
    } else
      aux++;
  }
  strcat(z->json, "]}");
  
  /* ADD DEVICE INFO */
  strcat(z->json, "],\"device\": \"");
  strcat(z->json, z->id);
  strcat(z->json, "\",\"timestamp\": \"");
  strcat(z->json, take_timetoa());
  strcat(z->json, "\"}");

#ifdef SHELLPRINT
  print_json(z->json); // Printf in shell
#endif
}

bool sendtoEndpoint(struct device *z) {
  bool b_socket = socket_sender(z->addr, z->addr_port, z->top, z->user_mqtt, z->pass_mqtt, z->json, z->interv);
  if (b_socket)
    led_blinks(0, 2, 60000); // Blink in green LED;
  else
    led_blinks(1, 3, 70000); // Blink in green RED - ERROR 1 (Bad connection with the endpoint);

  return b_socket;
}

void t_delay(long d, long l) {
  if (l >= d) /* To prevent crashes */
    l = d;
  udelay_basics((d - l) * 1000000); /* Time set by user  minus  loss time by operation */
}
